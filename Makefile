# This is the makefile for the CIMRefactorer driver

# Project Name
PROJECT_NAME = CIMRefactorer

# Set Compiler
CXX = clang++

# Set LLVM installation directory
LLVM_INSTALL_DIR=/usr/local/opt/llvm
BOOST_INSTALL_DIR=/usr/local

# Set DUMMY_STL
DUMMY_STL = /Users/daniel/Bachelorarbeit/Git/CIMRefactorer/resource/DummySTL

# STOP! All configuration done!

# Include directories
INCDIR =\
		-I$(LLVM_INSTALL_DIR)/include \
		-I$(BOOST_INSTALL_DIR)/include

# Libraries directories
LIBDIR =\
		-L$(LLVM_INSTALL_DIR)/lib \
		-L$(BOOST_INSTALL_DIR)/lib

# clang libs
# see llvm/tools/clang/tools/libclang/Makefile for any information about the linking order
CLANG_LIBS := \
		-lclangIndex \
		-lclangARCMigrate \
		-lclangRewriteFrontend \
		-lclangFormat \
		-lclangTooling \
		-lclangToolingCore \
		-lclangFrontend \
		-lclangCodeGen \
		-lclangDriver \
		-lclangSerialization \
		-lclangParse \
		-lclangSema \
		-lclangStaticAnalyzerCheckers \
		-lclangStaticAnalyzerCore \
		-lclangRewrite \
		-lclangAnalysis \
		-lclangEdit \
		-lclangASTMatchers \
		-lclangAST \
		-lclangLex \
		-lclangBasic

LIBS =\
		$(CLANG_LIBS) \
		-lboost_filesystem -lboost_system

# Compilerflags
CXXFLAGS =\
		$(INCDIR) -fPIC -fvisibility-inlines-hidden -Wall -W -Wno-unused-parameter -Wwrite-strings -Wcast-qual -Wmissing-field-initializers -pedantic -Wno-long-long -Wnon-virtual-dtor -Wdelete-non-virtual-dtor -std=c++11 -fno-rtti -D__STDC_CONSTANT_MACROS -D__STDC_FORMAT_MACROS -D__STDC_LIMIT_MACROS \
		-Wno-redundant-move \
		-Wno-strict-aliasing \
		-DDUMMY_STL=\"-I$(DUMMY_STL)\" -DCLANG_INC=\"-I$(LLVM_INSTALL_DIR)/lib/clang/$(shell $(LLVM_INSTALL_DIR)/bin/llvm-config --version)/include\" \
		-O3

# Linkerflags
LDFLAGS =\
		$(LIBDIR) $(LIBS)
LLVM_LDFLAGS =\
		`$(LLVM_INSTALL_DIR)/bin/llvm-config --ldflags --libs --system-libs`

# Directories
PROJDIR = .
SRCDIR = $(PROJDIR)/src
OBJDIR = $(PROJDIR)/obj
BINDIR = $(PROJDIR)/build
DOCDIR = $(PROJDIR)/documentation

OBJ = $(OBJDIR)/main.o $(OBJDIR)/myastvisitor.o $(OBJDIR)/myastconsumer.o  $(OBJDIR)/myastfrontendaction.o
BIN = $(BINDIR)/$(PROJECT_NAME)
DOXYFILE = $(PROJDIR)/Doxyfile

# First target which should be called to build
default: directories $(BIN) resources

# Build all including documentation
all: directories $(BIN) doc resources

# Directories target
.PHONY: directories
directories:
	@mkdir -p $(OBJDIR)
	@mkdir -p $(BINDIR)
	@mkdir -p $(DOCDIR)

# Clean target
.PHONY: clean
clean:
	@rm -rf $(OBJDIR)
	@rm -rf $(BINDIR)
	@rm -rf $(DOCDIR)
	@rm -f $(DOXYFILE)
	@echo Finished cleaning up

# Documentation target
.PHONY: doc
doc: $(DOXYFILE)
	doxygen $(DOXYFILE)

# Doxyfile
$(DOXYFILE):
	@echo PROJECT_NAME = "$(PROJECT_NAME)" > $(DOXYFILE)
	@echo OUTPUT_DIRECTORY = $(DOCDIR) >> $(DOXYFILE)
	@echo INPUT = $(SRCDIR) >> $(DOXYFILE)
	@echo GENERATE_LATEX = NO >> $(DOXYFILE)
	@echo QUIET = YES >> $(DOXYFILE)
	@echo Generated Doxyfile

# Pattern rule for source
$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	@$(CXX) $(CXXFLAGS) -c -o $@ $<
	@echo "Compiled $@"

# Linking pattern
$(BIN): $(OBJ)
	@$(CXX) -o $(BIN) $(OBJ) $(LDFLAGS) $(LLVM_LDFLAGS)
	@echo "Linked all objects to $@"

# Resources
.PHONY: resources
resources:
	@mkdir -p $(BINDIR)/resource
	@cp -rf $(PROJDIR)/resource/DummySTL $(BINDIR)/resource/DummySTL
