#include "myastvisitor.h"
#include <clang/Tooling/Refactoring.h> // clang::tooling::Replacements
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace clang;
using namespace clang::tooling;

extern Replacements* _Replacements;

MyASTVisitor::MyASTVisitor(const ASTContext& Context, const SourceManager& SrcManager)
	:	Context(Context),
		_SourceManager(SrcManager)
{
}

bool MyASTVisitor::VisitDecl(Decl *D)
{
	if(FieldDecl* fieldDecl = dyn_cast<FieldDecl>(D))
	{
		// Remove last "*" from "std::list<#TYPE#*> *Field;"
		if(fieldDecl->getType().getAsString().find("std::list") != std::string::npos)
		{
			auto offset = fieldDecl->getType().getAsString().find_last_of("*");
			Replacement Rep(_SourceManager, fieldDecl->getLocStart().getLocWithOffset(offset-1), 1, "");
			_Replacements->insert(Rep);
			//fieldDecl->dumpColor();
		}
		else if(fieldDecl->getType()->isEnumeralType())
		{
			std::string buffer(FullSourceLoc(fieldDecl->getLocStart(), _SourceManager).getCharacterData());
			buffer = buffer.substr(0, buffer.find("\n"));
			size_t offset = buffer.find("=");
			if(offset == std::string::npos)
			{
				offset = buffer.find(";");
				std::string rep = " = ";
				rep.append(fieldDecl->getType().getLocalUnqualifiedType().getAsString());
				rep.append("::_undef");
				Replacement Rep(_SourceManager, fieldDecl->getLocStart().getLocWithOffset(offset), 0, rep);
				_Replacements->insert(Rep);
			}
		}
	}

	if(EnumDecl* enumDecl = dyn_cast<EnumDecl>(D))
	{
		//TODO: Better Implementaion concerning Syntax (\v doesn't work) and if statement
		clang::EnumDecl::enumerator_iterator it = enumDecl->enumerator_begin();
		
		//std::cout << enumDecl->getNameAsString();
		
		//Insert enumerator _undef into every enum
		if(it != enumDecl->enumerator_end())
		{
			//for(clang::EnumDecl::enumerator_iterator enIt = it; enIt != enumDecl->enumerator_end(); enIt++)
			//{ it = enIt;}
			Replacement RepUndef( _SourceManager, (*it)->getLocStart().getLocWithOffset(-1), 0, "\t _undef = -1, ");
			_Replacements->insert(RepUndef);
		}
		else
		{
			Replacement RepUndef( _SourceManager, (enumDecl->getDefinition())->getLocEnd().getLocWithOffset(0), 0, "\t _undef = -1 ");
			_Replacements->insert(RepUndef);
			
		}
		// Insert "class" to any unscoped enum
		if(!enumDecl->isScoped())
		{
			Replacement Rep(_SourceManager, enumDecl->getLocStart().getLocWithOffset(5), 0, "class ");
			_Replacements->insert(Rep);
			//enumDecl->dumpColor();
		}		
	}


	// This will add an initialization list to constructors, which sets pointer fields to nullptr
	if(CXXConstructorDecl* ConstructorDecl = dyn_cast<CXXConstructorDecl>(D))
	{
		if(ConstructorDecl->hasBody())
		{
			std::vector<std::string> pointer_fields;
			// Parent is the class definition for the current constructor
			// Collect all pointer type fields
			for(FieldDecl* field : ConstructorDecl->getParent()->fields())
			{
				// Lines containing std::list are at this moment still pointer!
				if(field->getType()->isPointerType() && (field->getType().getAsString().find("std::list") == std::string::npos))
				{
					pointer_fields.push_back(field->getNameAsString());
				}
			}

			// Generate a replacement if needed
			if(!pointer_fields.empty())
			{
				std::string rep = "\n\t: ";
				for(size_t i = 0; i < pointer_fields.size(); i++)
				{
					rep.append(pointer_fields[i]);
					rep.append("(nullptr)");
					if(i < pointer_fields.size() - 1)
						rep.append(", ");
				}
				rep.append("\n{");

				size_t offset = std::string(FullSourceLoc(ConstructorDecl->getLocStart(), _SourceManager).getCharacterData()).find_first_of("{");
				Replacement Rep(_SourceManager, ConstructorDecl->getLocStart().getLocWithOffset(offset), 1, rep);
				_Replacements->insert(Rep);
			}
		}
	}

	// Find classes without BaseClass and add BaseClass
	if(CXXRecordDecl* RecordDeclaration = dyn_cast<CXXRecordDecl>(D))
	{
		if(RecordDeclaration->hasDefinition() && RecordDeclaration->getNumBases() == 0)
		{
			// Add BaseClass only if class is not on Whitelist!
			if(whitelist.find(RecordDeclaration->getQualifiedNameAsString()) == whitelist.end())
			{
				std::string buffer(FullSourceLoc(RecordDeclaration->getLocStart(), _SourceManager).getCharacterData());
				size_t offset = buffer.find("\n");
				buffer = buffer.substr(0,offset);
				if(buffer.find(':') == std::string::npos || buffer.find(';') == std::string::npos){
					Replacement Rep1(_SourceManager, RecordDeclaration->getLocStart().getLocWithOffset(offset), 0," : public BaseClass");
	                                _Replacements->insert(Rep1);
	 			
				}
				
				FileID SourceFileID = FullSourceLoc(RecordDeclaration->getLocStart(), _SourceManager).getFileID();
				offset = _SourceManager.getBufferData(SourceFileID).find("#include");
				if(offset == std::string::npos)
				{
					offset = _SourceManager.getBufferData(SourceFileID).find("#define");
					offset = _SourceManager.getBufferData(SourceFileID).find("\n", offset) + 1;
				}

				Replacement Rep2(_SourceManager, _SourceManager.getLocForStartOfFile(SourceFileID).getLocWithOffset(offset), 0, "#include \"BaseClass.h\"\n");
				_Replacements->insert(Rep2);
			}
		}
	}

	// Find lines like
	// const IEC61970::Base::Domain::UnitSymbol CapacitancePerLength::denominatorUnit = m;
	// and convert them to
	// const IEC61970::Base::Domain::UnitSymbol CapacitancePerLength::denominatorUnit = IEC61970::Base::Domain::UnitSymbol::m;
	if(VarDecl* VarDecl_ = dyn_cast<VarDecl>(D))
	{
		if(VarDecl_->getType()->isEnumeralType())
		{
			std::string buffer(FullSourceLoc(VarDecl_->getLocStart(), _SourceManager).getCharacterData());
			buffer = buffer.substr(0, buffer.find("\n"));
			size_t offset = buffer.find("=");
			if(offset != std::string::npos)
			{
				std::string rep = VarDecl_->getType().getLocalUnqualifiedType().getAsString();
				rep.append("::");
				Replacement Rep(_SourceManager, VarDecl_->getLocStart().getLocWithOffset(offset + 2), 0, rep);
				_Replacements->insert(Rep);
			}
		}
	}

	return true;
}

bool MyASTVisitor::VisitStmt(Stmt *stmt)
{
	return true;
}

// Load whitelist
std::unordered_set<std::string> MyASTVisitor::whitelist;

void MyASTVisitor::load_whitelist(const char* file_name)
{
	std::ifstream file;
	file.open(file_name, std::fstream::in);
	if(!file.good())
	{
		std::cerr << "Whitelist could not be loaded!" << std::endl;
		std::cerr << "File: " << file_name << " does not exist." << std::endl;
		exit(2);
	}
	std::string line;
	while (std::getline(file, line))
	{
		whitelist.insert(line);
	}
	return;
}
