#include "clang/Tooling/Refactoring.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"

#include "boost/filesystem.hpp"

#include "myastvisitor.h"
#include "myastconsumer.h"
#include "myastfrontendaction.h"

#include <iostream>
#include <fstream>

#define WRITE

// Globals
clang::tooling::Replacements* _Replacements;
std::unordered_set<std::string> files_set;
std::string binary_path;

std::string search_path;

int main(int argc, const char **argv)
{
	// Set current path
	binary_path = argv[0];
	binary_path =  binary_path.substr(0, binary_path.find_last_of("/\\") + 1);
	binary_path = boost::filesystem::absolute(binary_path).string();

	// Check for arguments
	if(argc <= 2)
	{
		std::cerr << "Too few arguments:\nUsage: CIMRefactorer <EACode_path> <whitelist> [clang_arguments...]" << std::endl;
		exit(1);
	}
	std::vector<std::string> files;
	search_path = argv[1];
	// Find all files in search_path
	boost::filesystem::path p(search_path);
	try
	{
		p = boost::filesystem::canonical(p);
		for(boost::filesystem::recursive_directory_iterator it(p); it != boost::filesystem::recursive_directory_iterator(); it++)
		{
			if(boost::filesystem::is_regular_file(*it) && (it->path().extension() == ".h" || it->path().extension() == ".cpp"))
			{
				files.push_back(it->path().string());
				files_set.insert(it->path().string());
			}
		}
	}
	catch(const boost::filesystem::filesystem_error& ex)
	{
		std::cout << ex.what() << std::endl;
	}

	// Prepare compilation database
	std::vector<std::string> args;
	args.push_back(std::string("-I").append(p.string()));
	args.push_back("-I" + binary_path + "resource/DummySTL");
	args.push_back(CLANG_INC);
	args.push_back("-xc++");
	args.push_back("-std=c++11");
	for (int i = 3; i < argc; i++) {
		args.push_back(argv[i]);
	}

	clang::tooling::FixedCompilationDatabase db(argv[1], args);
	clang::tooling::RefactoringTool Tool(db, files);

	MyASTVisitor::load_whitelist(argv[2]);

	_Replacements = &Tool.getReplacements();
	llvm::outs() << "Running tool with RecursiveASTVisitor\n";
	Tool.run(clang::tooling::newFrontendActionFactory<MyASTFrontendAction>().get());

	if(!Tool.getReplacements().empty())
		llvm::outs() << "Replacements collected by the tool:\n";
	else
		llvm::outs() << "No replacements generated\n";
	for (auto &r : Tool.getReplacements())
	{
	  llvm::outs() << r.toString() << "\n";
	}

#ifdef WRITE
	clang::IntrusiveRefCntPtr<clang::DiagnosticOptions> DiagOpts = new clang::DiagnosticOptions();
	clang::TextDiagnosticPrinter DiagnosticPrinter(llvm::errs(), &*DiagOpts);
	clang::DiagnosticsEngine Diagnostics(clang::IntrusiveRefCntPtr<clang::DiagnosticIDs>(new clang::DiagnosticIDs()), &*DiagOpts, &DiagnosticPrinter, false);
	clang::SourceManager srcMgr(Diagnostics, Tool.getFiles());
	clang::Rewriter Rewrite(srcMgr, clang::LangOptions());

	if (!clang::tooling::applyAllReplacements(Tool.getReplacements(), Rewrite))
	{
		llvm::errs() << "Skipped some replacements.\n";
	}
	Rewrite.overwriteChangedFiles();
#endif // WRITE

	return 0;
}
