#include "myastfrontendaction.h"
#include "myastconsumer.h"

#include <iostream>

using namespace clang;
using namespace llvm;

MyASTFrontendAction::MyASTFrontendAction()
{
}

std::unique_ptr<ASTConsumer> MyASTFrontendAction::CreateASTConsumer(CompilerInstance &CI, StringRef file)
{
	CI.getDiagnostics().setClient(new IgnoringDiagConsumer());
	return llvm::make_unique<MyASTConsumer>(CI.getASTContext(), CI.getSourceManager());
}
