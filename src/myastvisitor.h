#ifndef MYASTVISITOR_H
#define MYASTVISITOR_H

#include <clang/AST/RecursiveASTVisitor.h>
#include <unordered_set>

// The visitor approach
class MyASTVisitor : public clang::RecursiveASTVisitor<MyASTVisitor>
{
public:

	const clang::ASTContext& Context;
	const clang::SourceManager& _SourceManager;

	static void load_whitelist(const char* file_name);

	explicit MyASTVisitor(const clang::ASTContext& Context, const clang::SourceManager& SrcManager);
	bool VisitDecl(clang::Decl *D);
	bool VisitStmt(clang::Stmt *stmt);

	static std::unordered_set<std::string> whitelist;
};

#endif // MYASTVISITOR_H
